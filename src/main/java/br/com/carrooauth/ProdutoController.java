package br.com.carrooauth;

import br.com.carrooauth.Seguraca.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProdutoController {

    @GetMapping("/{nomeProduto}")
    public Produto criar (@PathVariable String nomeProduto, @AuthenticationPrincipal Usuario usuario){
        Produto produto = new Produto();
        produto.setNomeProduto(nomeProduto);
        produto.setProprietario(usuario.getName());
        return produto;
    }

}
