package br.com.carrooauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarroOauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarroOauthApplication.class, args);
	}

}
